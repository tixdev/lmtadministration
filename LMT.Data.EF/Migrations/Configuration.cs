using System.Data.Entity.Migrations;

namespace LMT.Data.EF.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Entities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(Entities context)
        {
            var adminRole = new Role {Name = Role.Administrator};
            var userRole = new Role {Name = Role.User};
            context.Roles.AddOrUpdate(r => r.Name, adminRole, userRole);

            var admin = new User {AccountName = "admin", Password = "admin"};
            admin.Roles.Add(adminRole);

            context.Users.AddOrUpdate(u => u.AccountName, admin);

            context.MaintenanceStates.AddOrUpdate(m => m.Code, new MaintenanceState { Code = "10", Name = "Preventivo", DisplayOrder = 10 },
                                                  new MaintenanceState { Code = "20", Name = "Attesa ricambi", DisplayOrder = 20 },
                                                  new MaintenanceState { Code = "30", Name = "Pronto", DisplayOrder = 30 },
                                                  new MaintenanceState { Code = "40", Name = "Ritirato", DisplayOrder = 50 },
                                                  new MaintenanceState { Code = "50", Name = "Avvisato", DisplayOrder = 40 },
                                                  new MaintenanceState { Code = "60", Name = "Chiuso", DisplayOrder = 60 },
                                                  new MaintenanceState { Code = "70", Name = "Sospeso", DisplayOrder = 7 },
                                                  new MaintenanceState { Code = "80", Name = "Preventivo comunicato", DisplayOrder = 15 },
                                                  new MaintenanceState { Code = "5", Name = "Nuovo", DisplayOrder = 5 },
                                                  new MaintenanceState { Code = "90", Name = "Ricambi ordinati", DisplayOrder = 25 },
                                                  new MaintenanceState { Code = "100", Name = "Pronto 2", DisplayOrder = 35 });
        }
    }
}