namespace LMT.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_ComponentEntry_Added_Note : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ComponentEntries", "Note", c => c.String(maxLength: 500));
            AddColumn("dbo.ComponentGroups", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.ComponentGroups", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("dbo.ComponentGroups", "UpdatedOn", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ComponentGroups", "UpdatedOn");
            DropColumn("dbo.ComponentGroups", "CreatedOn");
            DropColumn("dbo.ComponentGroups", "Deleted");
            DropColumn("dbo.ComponentEntries", "Note");
        }
    }
}
