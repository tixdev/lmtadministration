﻿Commands:

//Local
enable-migrations
update-database -startupprojectname LMT.Data.EF -verbose
add-migration /*MigrationName*/ -startupprojectname LMT.Data.EF -verbose

//Prod
update-database -startupprojectname LMT.Data.EF -connectionstringname LmtDbProd -verbose