namespace LMT.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updated_QuantitiesToDecimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Components", "StockQuantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.ComponentEntries", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ComponentEntries", "Quantity", c => c.Int(nullable: false));
            AlterColumn("dbo.Components", "StockQuantity", c => c.Int(nullable: false));
        }
    }
}
