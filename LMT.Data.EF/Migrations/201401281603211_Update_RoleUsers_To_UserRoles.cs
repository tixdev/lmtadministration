namespace LMT.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_RoleUsers_To_UserRoles : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.RoleUsers", newName: "UserRoles");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.UserRoles", newName: "RoleUsers");
        }
    }
}
