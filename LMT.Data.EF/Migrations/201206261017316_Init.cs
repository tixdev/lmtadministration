namespace LMT.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Code = c.String(maxLength: 50),
                        Quantity = c.Decimal(precision: 18, scale: 2),
                        Description = c.String(maxLength: 500),
                        ListPrice = c.Decimal(precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Price = c.Decimal(precision: 18, scale: 2),
                        Notes = c.String(maxLength: 500),
                        InsertionDate = c.DateTime(nullable: false),
                        EndingDate = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Brand_Id = c.Int(),
                        Type_Id = c.Int(),
                        VatRate_Id = c.Int(),
                        MeasureUnit_Id = c.Int(),
                        Category_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brands", t => t.Brand_Id)
                .ForeignKey("dbo.Types", t => t.Type_Id)
                .ForeignKey("dbo.VatRates", t => t.VatRate_Id)
                .ForeignKey("dbo.MeasureUnits", t => t.MeasureUnit_Id)
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .Index(t => t.Brand_Id)
                .Index(t => t.Type_Id)
                .Index(t => t.VatRate_Id)
                .Index(t => t.MeasureUnit_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.Brands",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 500),
                        Notes = c.String(maxLength: 500),
                        InsertionDate = c.DateTime(nullable: false),
                        EndingDate = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Types",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 500),
                        Notes = c.String(maxLength: 500),
                        InsertionDate = c.DateTime(nullable: false),
                        EndingDate = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VatRates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(maxLength: 500),
                        Notes = c.String(maxLength: 500),
                        InsertionDate = c.DateTime(nullable: false),
                        EndingDate = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MeasureUnits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Symbol = c.String(nullable: false),
                        Description = c.String(maxLength: 500),
                        Notes = c.String(maxLength: 500),
                        InsertionDate = c.DateTime(nullable: false),
                        EndingDate = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 500),
                        Notes = c.String(maxLength: 500),
                        InsertionDate = c.DateTime(nullable: false),
                        EndingDate = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Products", new[] { "Category_Id" });
            DropIndex("dbo.Products", new[] { "MeasureUnit_Id" });
            DropIndex("dbo.Products", new[] { "VatRate_Id" });
            DropIndex("dbo.Products", new[] { "Type_Id" });
            DropIndex("dbo.Products", new[] { "Brand_Id" });
            DropForeignKey("dbo.Products", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.Products", "MeasureUnit_Id", "dbo.MeasureUnits");
            DropForeignKey("dbo.Products", "VatRate_Id", "dbo.VatRates");
            DropForeignKey("dbo.Products", "Type_Id", "dbo.Types");
            DropForeignKey("dbo.Products", "Brand_Id", "dbo.Brands");
            DropTable("dbo.Categories");
            DropTable("dbo.MeasureUnits");
            DropTable("dbo.VatRates");
            DropTable("dbo.Types");
            DropTable("dbo.Brands");
            DropTable("dbo.Products");
        }
    }
}
