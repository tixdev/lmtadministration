namespace LMT.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Components : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Components",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 200),
                        Deleted = c.Boolean(nullable: false),
                        Sku = c.String(nullable: false, maxLength: 100),
                        StockQuantity = c.Int(nullable: false),
                        NotifyForQuantityBelow = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ComponentEntries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Component_Id = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Components", t => t.Component_Id)
                .Index(t => t.Component_Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ComponentEntries", new[] { "Component_Id" });
            DropForeignKey("dbo.ComponentEntries", "Component_Id", "dbo.Components");
            DropTable("dbo.ComponentEntries");
            DropTable("dbo.Components");
        }
    }
}
