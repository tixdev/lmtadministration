namespace LMT.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updated_QuantitiesToDecimal2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ComponentGroupComponents", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ComponentGroupComponents", "Quantity", c => c.Int(nullable: false));
        }
    }
}
