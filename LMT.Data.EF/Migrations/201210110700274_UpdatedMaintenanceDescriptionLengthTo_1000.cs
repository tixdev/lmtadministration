namespace LMT.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedMaintenanceDescriptionLengthTo_1000 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MaintenanceItems", "Description", c => c.String(nullable: false, maxLength: 1000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MaintenanceItems", "Description", c => c.String(nullable: false, maxLength: 300));
        }
    }
}
