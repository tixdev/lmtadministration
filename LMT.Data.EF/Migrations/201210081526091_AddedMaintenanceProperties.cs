namespace LMT.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMaintenanceProperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Maintenances", "UpdateDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Maintenances", "WarrantyStartDate", c => c.DateTime());
            AlterColumn("dbo.MaintenanceItems", "Description", c => c.String(nullable: false, maxLength: 300));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MaintenanceItems", "Description", c => c.String(nullable: false, maxLength: 150));
            DropColumn("dbo.Maintenances", "WarrantyStartDate");
            DropColumn("dbo.Maintenances", "UpdateDate");
        }
    }
}
