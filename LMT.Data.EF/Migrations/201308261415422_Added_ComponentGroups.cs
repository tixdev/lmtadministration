namespace LMT.Data.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_ComponentGroups : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ComponentGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ComponentGroupComponents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        Component_Id = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ComponentGroup_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Components", t => t.Component_Id)
                .ForeignKey("dbo.ComponentGroups", t => t.ComponentGroup_Id)
                .Index(t => t.Component_Id)
                .Index(t => t.ComponentGroup_Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ComponentGroupComponents", new[] { "ComponentGroup_Id" });
            DropIndex("dbo.ComponentGroupComponents", new[] { "Component_Id" });
            DropForeignKey("dbo.ComponentGroupComponents", "ComponentGroup_Id", "dbo.ComponentGroups");
            DropForeignKey("dbo.ComponentGroupComponents", "Component_Id", "dbo.Components");
            DropTable("dbo.ComponentGroupComponents");
            DropTable("dbo.ComponentGroups");
        }
    }
}
