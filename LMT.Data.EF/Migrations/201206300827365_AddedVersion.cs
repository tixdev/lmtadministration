using System.Data.Entity.Migrations;

namespace LMT.Data.EF.Migrations
{
    public partial class AddedVersion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Versions",
                c => new
                         {
                             Id = c.Int(nullable: false, identity: true),
                             Name = c.String(),
                             DateTime = c.DateTime(nullable: false),
                             RowVersion =
                         c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                         })
                .PrimaryKey(t => t.Id);

            AlterColumn("dbo.Products", "Discount", c => c.Decimal(precision: 18, scale: 2));
        }

        public override void Down()
        {
            AlterColumn("dbo.Products", "Discount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropTable("dbo.Versions");
        }
    }
}