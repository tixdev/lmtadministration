﻿using System;

namespace LMT.Data.EF
{
    public class Version : EntityObject
    {
        public string Name { get; set; }
        public DateTime DateTime { get; set; }
    }
}
