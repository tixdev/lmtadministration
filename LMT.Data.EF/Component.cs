﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LMT.Data.EF
{
    public class Component : EntityObject
    {
        public Component()
        {
            ComponentEntries = new List<ComponentEntry>();
        }
        
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Description { get; set; }
        public bool Deleted { get; set; }

        [Required]
        [StringLength(100)]
        public string Sku { get; set; }
        public decimal StockQuantity { get; set; }
        public int NotifyForQuantityBelow { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }                
        public virtual ICollection<ComponentEntry> ComponentEntries { get; set; }
    }
}