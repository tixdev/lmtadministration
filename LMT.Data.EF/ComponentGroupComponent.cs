﻿using System.ComponentModel.DataAnnotations;

namespace LMT.Data.EF
{
    public class ComponentGroupComponent : EntityObject
    {
        public decimal Quantity { get; set; }

        [Required]
        internal int ComponentId { get; set; }
        public virtual Component Component { get; set; }
    }
}