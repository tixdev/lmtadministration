﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using LMT.Data.EF.Mapping;
using LMT.Data.EF.Migrations;

namespace LMT.Data.EF
{
    public class Entities : DbContext
    {
        static Entities()
        {
            //Database.SetInitializer<Entities>(null);
            Database.SetInitializer<Entities>(new MigrateDatabaseToLatestVersion<Entities, Configuration>());
        }

        public Entities()
            : base("Name=LmtDb")
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Type> Types { get; set; }
        public DbSet<VatRate> VatRates { get; set; }
        public DbSet<MeasureUnit> MeasureUnits { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Version> Versions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Maintenance> Maintenances { get; set; }
        public DbSet<MaintenanceState> MaintenanceStates { get; set; }
        public DbSet<MaintenanceItem> MaintenanceItems { get; set; }
        public DbSet<Component> Components { get; set; }
        public DbSet<ComponentEntry> ComponentEntries { get; set; }
        public DbSet<ComponentGroup> ComponentGroups { get; set; }
        public DbSet<ComponentGroupComponent> ComponentGroupComponents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new ProductMap());
            modelBuilder.Configurations.Add(new BrandMap());
            modelBuilder.Configurations.Add(new TypeMap());
            modelBuilder.Configurations.Add(new VatRateMap());
            modelBuilder.Configurations.Add(new MeasureUnitMap());
            modelBuilder.Configurations.Add(new CategoryMap());
            modelBuilder.Configurations.Add(new VersionMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new MaintenanceMap());
            modelBuilder.Configurations.Add(new MaintenanceItemMap());
            modelBuilder.Configurations.Add(new MaintenanceStateMap());
            modelBuilder.Configurations.Add(new ComponentMap());
            modelBuilder.Configurations.Add(new ComponentEntryMap());
            modelBuilder.Configurations.Add(new ComponentGroupComponentMap());
            modelBuilder.Configurations.Add(new ComponentGroupMap());
        }
    }
}