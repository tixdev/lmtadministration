﻿using System.ComponentModel.DataAnnotations;

namespace LMT.Data.EF
{
    public class MaintenanceItem : EntityObject
    {
        [Required]
        [StringLength(1000)]
        public string Description { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        internal int MaintenanceId { get; set; }        
        public virtual Maintenance Maintenance { get; set; }
    }
}
