using System.ComponentModel.DataAnnotations;

namespace LMT.Data.EF
{
    public class MaintenanceState : EntityObject
    {
        public const string Estimate = "10";        
        public const string WaitingForParts = "20";
        public const string Ready = "30";
        public const string Delivered = "40";
        public const string Advised = "50";
        public const string Invoice = "60";
        public const string New = "5";

        [Required]
        public string Code { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public int DisplayOrder { get; set; }
    }
}