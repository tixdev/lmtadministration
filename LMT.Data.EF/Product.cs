﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LMT.Data.EF
{
    public class Product : EntityObject
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(50)]
        public string Code { get; set; }
        public virtual Brand Brand { get; set; }
        public decimal? Quantity { get; set; }
        public virtual Type Type { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public decimal? ListPrice { get; set; }
        public decimal? Discount { get; set; }
        public virtual VatRate VatRate { get; set; }
        public decimal? Price { get; set; }
        public virtual MeasureUnit MeasureUnit { get; set; }
        public virtual Category Category { get; set; }
        [StringLength(500)]
        public string Notes { get; set; }
        [Required]
        public DateTime InsertionDate { get; set; }
        [Required]
        public DateTime EndingDate { get; set; }
    }
}
