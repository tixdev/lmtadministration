﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace LMT.Data.EF
{
    public class User : EntityObject
    {
        public User()
        {
            Roles = new Collection<Role>();
        }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(50)]
        [Required]
        public string AccountName { get; set; }

        [StringLength(20)]
        [Required]
        public string Password { get; set; }

        [Required]
        public bool Deleted { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
