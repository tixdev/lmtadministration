﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace LMT.Data.EF
{
    public class Role : EntityObject
    {
        public const string Administrator = "Administrator";
        public const string ContentManager = "Content Manager";
        public const string User = "User";
        public const string Viewer = "Viewer";

        public Role()
        {
            Users = new Collection<User>();
        }

        [StringLength(100)]
        [Required]
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
