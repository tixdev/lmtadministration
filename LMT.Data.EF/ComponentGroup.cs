﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace LMT.Data.EF
{
    public class ComponentGroup : EntityObject
    {
        public ComponentGroup()
        {
            ComponentGroupComponents = new Collection<ComponentGroupComponent>();
        }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public bool Deleted { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public virtual ICollection<ComponentGroupComponent> ComponentGroupComponents { get; set; }
    }
}
