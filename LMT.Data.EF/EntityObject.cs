﻿namespace LMT.Data.EF
{
    public class EntityObject
    {
        public int Id { get; set; }
        public byte[] RowVersion { get; set; }  
    }
}
