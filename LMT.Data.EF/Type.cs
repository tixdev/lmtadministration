﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LMT.Data.EF
{
    public class Type : EntityObject
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        [StringLength(500)]
        public string Notes { get; set; }
        [Required]
        public DateTime InsertionDate { get; set; }
        [Required]
        public DateTime EndingDate { get; set; }
    }
}
