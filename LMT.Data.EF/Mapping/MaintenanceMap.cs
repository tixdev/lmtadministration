﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    public class MaintenanceMap : EntityTypeConfiguration<Maintenance>
    {
        public MaintenanceMap()
        {
            Property(m => m.RowVersion).IsRowVersion();            
            HasMany(m => m.MaintenanceItems).WithRequired(m => m.Maintenance).HasForeignKey(m => m.MaintenanceId);
        }
    }
}
