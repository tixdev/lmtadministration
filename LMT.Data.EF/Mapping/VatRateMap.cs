﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    class VatRateMap:EntityTypeConfiguration<VatRate>
    {
        public VatRateMap()
        {
            Property(v => v.RowVersion).IsRowVersion();
        }
    }
}
