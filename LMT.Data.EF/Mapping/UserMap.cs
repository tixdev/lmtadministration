﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            Property(u => u.RowVersion).IsRowVersion();

            HasMany(u => u.Roles);
        }
    }
}