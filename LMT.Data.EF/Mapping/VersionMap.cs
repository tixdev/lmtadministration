﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    class VersionMap: EntityTypeConfiguration<Version>
    {
        public VersionMap()
        {
            Property(b => b.RowVersion).IsRowVersion();
        }
    }
}
