﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    public class ComponentGroupMap : EntityTypeConfiguration<ComponentGroup>
    {
        public ComponentGroupMap()
        {
            Property(p => p.RowVersion).IsRowVersion();
        }
    }
}
