﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    public class ComponentGroupComponentMap : EntityTypeConfiguration<ComponentGroupComponent>
    {
        public ComponentGroupComponentMap()
        {
            Property(p => p.RowVersion).IsRowVersion();

            Property(p => p.ComponentId).IsRequired().HasColumnName("Component_Id");

            HasRequired(p => p.Component).WithMany().HasForeignKey(c => c.ComponentId);
        }
    }
}
