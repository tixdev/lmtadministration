﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    class BrandMap:EntityTypeConfiguration<Brand>
    {
        public BrandMap()
        {
            Property(b => b.RowVersion).IsRowVersion();
        }
    }
}
