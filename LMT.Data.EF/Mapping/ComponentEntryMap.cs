﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    public class ComponentEntryMap : EntityTypeConfiguration<ComponentEntry>
    {
        public ComponentEntryMap()
        {
            Property(p => p.RowVersion).IsRowVersion();

            Property(c => c.ComponentId).IsRequired().HasColumnName("Component_Id");
            HasRequired(c => c.Component).WithMany(c => c.ComponentEntries).HasForeignKey(c => c.ComponentId);
        }
    }
}
