﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    public class CategoryMap : EntityTypeConfiguration<Category>
    {
        public CategoryMap()
        {
            Property(c => c.RowVersion).IsRowVersion();
        }
    }
}
