﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    public class ProductMap : EntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            Property(p => p.RowVersion).IsRowVersion();
        }
    }
}
