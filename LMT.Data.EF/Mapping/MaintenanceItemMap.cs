﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    public class MaintenanceItemMap : EntityTypeConfiguration<MaintenanceItem>
    {
        public MaintenanceItemMap()
        {
            Property(m => m.RowVersion).IsRowVersion();
            Property(m => m.MaintenanceId).IsRequired().HasColumnName("Maintenance_Id");
            HasRequired(m => m.Maintenance).WithMany(m => m.MaintenanceItems).HasForeignKey(m => m.MaintenanceId);
        }
    }
}
