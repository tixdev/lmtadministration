﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    class TypeMap:EntityTypeConfiguration<Type>
    {
        public TypeMap()
        {
            Property(t => t.RowVersion).IsRowVersion();
        }
    }
}
