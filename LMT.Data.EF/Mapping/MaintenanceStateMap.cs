﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    public class MaintenanceStateMap : EntityTypeConfiguration<MaintenanceState>
    {
        public MaintenanceStateMap()
        {
            Property(m => m.RowVersion).IsRowVersion();            
        }
    }
}
