﻿using System.Data.Entity.ModelConfiguration;

namespace LMT.Data.EF.Mapping
{
    class MeasureUnitMap:EntityTypeConfiguration<MeasureUnit>
    {
        public MeasureUnitMap()
        {
            Property(m => m.RowVersion).IsRowVersion();
        }
    }
}
