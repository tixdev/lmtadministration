﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace LMT.Data.EF
{
    public class Maintenance : EntityObject
    {
        public Maintenance()
        {
            MaintenanceItems = new Collection<MaintenanceItem>();            
        }

        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime? WarrantyStartDate { get; set; }

        [StringLength(100)]
        public string CustomerName { get; set; }

        [StringLength(100)]
        public string CustomerPhone { get; set; }

        [StringLength(100)]
        public string BrandName { get; set; }

        [StringLength(100)]
        public string ProductSku { get; set; }

        [StringLength(100)]
        public string ModelNumber { get; set; }

        [StringLength(200)]
        public string HandWorkHours { get; set; }

        [Required]
        public decimal HandWorkPrice { get; set; }

        [StringLength(500)]
        public string Notes { get; set; }

        public bool Deleted { get; set; }

        public virtual MaintenanceState MaintenanceState { get; set; }

        public virtual ICollection<MaintenanceItem> MaintenanceItems { get; set; }        
    }
}
