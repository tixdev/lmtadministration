﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LMT.Data.EF
{
    public class ComponentEntry : EntityObject
    {        
        [Required]
        internal int ComponentId { get; set; }
        public virtual Component Component { get; set; }

        public decimal Quantity { get; set; }                

        public DateTime CreatedOn { get; set; }

        [StringLength(500)]
        public string Note { get; set; }
    }
}