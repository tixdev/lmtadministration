﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.WebPages;
using LMT.Web.Application.App_Start;

namespace LMT.Web.Application
{    
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            DisplayModeProvider.Instance.Modes.Insert(0, new MobileDisplayMode());
        }

        protected void Application_BeginRequest()
        {
            InitializeCustomCultureFormats();
        }

        private static void InitializeCustomCultureFormats()
        {
            var numberFormat = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = "",
                NumberDecimalDigits = 2,
                NumberGroupSizes = new[] { 3 }
            };

            const string dateShortPattern = "dd-MM-yyyy";

            Thread.CurrentThread.CurrentCulture =
                new CultureInfo(Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName)
                    {NumberFormat = numberFormat, DateTimeFormat = {ShortDatePattern = dateShortPattern}};

            Thread.CurrentThread.CurrentUICulture =
                new CultureInfo(Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName)
                    {NumberFormat = numberFormat, DateTimeFormat = {ShortDatePattern = dateShortPattern}};
        }
    }

    public class MobileDisplayMode : DefaultDisplayMode
    {
        private readonly StringCollection _useragenStringPartialIdentifiers = new StringCollection
                                                                                  {
                                                                                      "Android",
                                                                                      "Mobile",
                                                                                      "Opera Mobi",
                                                                                      "Samsung",
                                                                                      "HTC",
                                                                                      "Nokia",
                                                                                      "Ericsson",
                                                                                      "SonyEricsson",
                                                                                      "iPhone"
                                                                                  };

        public MobileDisplayMode()
            : base("Mobile")
        {
            ContextCondition = (context => IsMobile(context.GetOverriddenUserAgent()));
        }

        private bool IsMobile(string useragentString)
        {
            return _useragenStringPartialIdentifiers.Cast<string>()
                        .Any(val => useragentString.IndexOf(val, StringComparison.InvariantCultureIgnoreCase) >= 0);
        }
    }
}