﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using LMT.Data.EF;

namespace LMT.Web.Application.Security
{
    public class LmtRoleProvider : RoleProvider
    {
        public override string ApplicationName
        {
            get { return "/"; }
            set { }
        }

        public static string LoggedUserName
        {
            get { return HttpContext.Current.User.Identity.Name; }
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            var entities = DependencyResolver.Current.GetService<Entities>();

            var currentUser =
                entities.Users.Single(u => !u.Deleted && u.AccountName == LoggedUserName);

            return currentUser.Roles.Any(r => r.Name == roleName);
        }

        public override string[] GetRolesForUser(string username)
        {
            var entities = DependencyResolver.Current.GetService<Entities>();

            var currentUser =
                entities.Users.Single(u => !u.Deleted && u.AccountName == LoggedUserName);

            return currentUser != null ? currentUser.Roles.Select(r => r.Name).ToArray() : new string[] {};
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }
    }
}