﻿var lmt = lmt || {};

lmt.CheckViewModel = function (args) {

    this.componentGroupElements = ko.observableArray([]);
    this.componentGroups = ko.observableArray([]);
    this.componentsCurrentAvailability = ko.observableArray([]);
    this.componentsNeedAvailability = ko.observableArray([]);
    this.componentsRemainingAvailability = ko.observableArray([]);

    this.addComponentGroupElement = function () {
        this.componentGroupElements.push({ selectedComponentGroupId: this.componentGroups()[0].value, quantity: 1 });
        
        $('select').selectmenu();
    };

    this.calculateAvailability = function () {

        var self = this;        

        $.ajax({
            url: args.checkAvailabilityAddress,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(self.componentGroupElements()),
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                self.componentsCurrentAvailability(result.componentsAvailability);
                self.componentsNeedAvailability(result.componentsNeedAvailability);
                self.componentsRemainingAvailability(result.componentsRemainingAvailability);
            }
        });        
    };

    this.removeQuantities = function () {

        var self = this;        

        $.ajax({
            url: args.removeQuantitiesAddress,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(self.componentGroupElements()),
            contentType: 'application/json; charset=utf-8',
            success: function (result) {                
                $('.ui-dialog').dialog('close');
                self.calculateAvailability();
            }
        });        
    };

    this.onComplete = function() {
        
        //$('#ctrlCheck').trigger('create');
    };

    this.initialize = function () {

        var self = this;

        $.get(args.componentsAddress)
            .done(function (data) {
                self.componentGroups(data);

                self.componentGroupElements.push(
                    { selectedComponentGroupId: self.componentGroups()[0].value, quantity: 1 });

                $('select').selectmenu();
            });
    };
};