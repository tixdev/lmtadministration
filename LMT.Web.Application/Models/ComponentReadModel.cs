﻿namespace LMT.Web.Application.Models
{
    public class ComponentReadModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
        public string StockQuantity { get; set; }
    }
}