﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMT.Data.EF;

namespace LMT.Web.Application.Models
{
    public class ComponentGroupReadModel
    {
        public ComponentGroupReadModel()
        {
            ComponentGroupComponents = new List<ComponentGroupComponent>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<ComponentGroupComponent> ComponentGroupComponents { get; set; }

        public string GetComponents()
        {
            return
                ComponentGroupComponents.Aggregate("",
                    (current, next) =>
                        string.Format("{0}{1}({2})", string.IsNullOrWhiteSpace(current) ? "" : current + ", ",
                            next.Component.Sku,
                            next.Quantity));
        }
    }
}