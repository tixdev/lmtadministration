﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LMT.Web.Application.Models
{
    public class ComponentEditModel
    {
        public int Id { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(50)]        
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        [Remote("IsValidSku", "Components", AdditionalFields = "Id", HttpMethod = "POST", ErrorMessage = "Codice esistente")]
        public string Sku { get; set; }               
    }
}