﻿using System.ComponentModel.DataAnnotations;

namespace LMT.Web.Application.Models
{
    public class ComponentGroupCreateModel
    {
        [Required]
        [StringLength(255)]
        public string Name { get; set; }        
    }
}