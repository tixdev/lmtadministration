﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace LMT.Web.Application.Models
{
    public class CheckComponentsAvaiabilityModel
    {
        public IEnumerable<SelectListItem> Components { get; set; }
    }
}