﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace LMT.Web.Application.Models
{
    public class MaintenanceSearchModel
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Brand { get; set; }
        public string CustomerName { get; set; }
        public string Model { get; set; }
        public string Sku { get; set; }        
        public string ExcludeClosedStates { get; set; }
        public string SelectedMaintenanceState { get; set; }
        public List<SelectListItem> MaintenanceStates { get; set; }
        public IEnumerable<MaintenanceReadModel> SearchResultModels { get; set; }
    }
}