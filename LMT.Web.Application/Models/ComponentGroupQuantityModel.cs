﻿namespace LMT.Web.Application.Models
{
    public class ComponentGroupQuantityModel
    {
        public int SelectedComponentGroupId { get; set; }
        public int Quantity { get; set; }
    }
}