﻿using System.ComponentModel.DataAnnotations;

namespace LMT.Web.Application.Models
{
    public class MaintenanceItemModel
    {
        [StringLength(1000, ErrorMessage = "La lunghezza massima consentita e' {1} caratteri")]
        public string MaintenanceDescription { get; set; }

        [RegularExpression(@"^(((([0-9]{1,4},)([0-9]{3},)*[0-9]{3})|[0-9]{1,4})(\.[0-9]+)?)$", ErrorMessage = "Formato non valido")]
        public string MaintenancePrice { get; set; }
    }
}