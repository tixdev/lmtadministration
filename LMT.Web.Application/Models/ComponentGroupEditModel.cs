﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace LMT.Web.Application.Models
{
    public class ComponentGroupEditModel
    {
        public ComponentGroupEditModel()
        {
            ComponentGroupComponentModels = new List<ComponentGroupComponentModel>();
        }

        public int Id { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public IEnumerable<SelectListItem> Components { get; set; }

        public List<ComponentGroupComponentModel> ComponentGroupComponentModels { get; set; }

        public IEnumerable<SelectListItem> GetComponents(int selected)
        {
            return
                Components.Select(
                    c => new SelectListItem {Selected = selected.ToString() == c.Value, Text = c.Text, Value = c.Value});
        }
    }
}