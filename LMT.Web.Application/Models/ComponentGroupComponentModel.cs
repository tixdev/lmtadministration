﻿using System.ComponentModel.DataAnnotations;

namespace LMT.Web.Application.Models
{
    public class ComponentGroupComponentModel
    {
        [Required]
        [RegularExpression(@"^-?(([1-9]\d*)|0)(.0*[1-9](0*[1-9])*)?$", ErrorMessage = "Formato non valido")]
        public string Quantity { get; set; }
        
        public int ComponentId { get; set; }

        public string Name { get; set; }

        public string Sku { get; set; }
    }
}