﻿using System.Collections.Generic;

namespace LMT.Web.Application.Models
{
    public class ComponentSearchModel
    {
        public ComponentSearchModel()
        {
            SearchResultModels = new List<ComponentReadModel>();
            ComponentGroupReadModels = new List<ComponentGroupReadModel>();
        }

        public string Name { get; set; }
        public string Sku { get; set; }

        public IEnumerable<ComponentReadModel> SearchResultModels { get; set; }
        public IEnumerable<ComponentGroupReadModel> ComponentGroupReadModels { get; set; }
    }
}