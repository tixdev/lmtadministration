﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LMT.Web.Application.Models
{
    public class MaintenanceCreateModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string CreationDate { get; set; }

        [Required(ErrorMessage = "Inserire il nome dell'azienda")]
        [StringLength(100)]
        public string Customer { get; set; }

        [Required(ErrorMessage = "Inserire la marca")]
        public string BrandName { get; set; }
        
        [Required(ErrorMessage = "Selezionare uno stato")]
        public int? SelectedMaintenanceState { get; set; }

        [StringLength(100)]
        public string Model { get; set; }

        [StringLength(100)]
        public string Sku { get; set; }
        
        public string HandWorkHours { get; set; }

        [RegularExpression(@"^(((([0-9]{1,4},)([0-9]{3},)*[0-9]{3})|[0-9]{1,4})(\.[0-9]+)?)$", ErrorMessage = "Formato non valido")]
        public string HandWorkPrice { get; set; }

        public IEnumerable<SelectListItem> Brands { get; set; }
        public List<SelectListItem> MaintenanceStates { get; set; }
        public List<MaintenanceItemModel> MaintenanceItemModels { get; set; }
    }
}