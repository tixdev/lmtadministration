﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LMT.Web.Application.Models
{
    public class ComponentCreateModel
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        [Remote("IsValidSku", "Components", HttpMethod = "POST", ErrorMessage = "Codice esistente")]
        public string Sku { get; set; }
    }
}