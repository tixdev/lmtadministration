﻿namespace LMT.Web.Application.Models
{
    public class MaintenanceReadModel
    {
        public int Id { get; set; }
        public string CreationDate { get; set; }
        public string UpdatedOn { get; set; }
        public string CustomerName { get; set; }
        public string ModelNumber { get; set; }
        public string BrandName { get; set; }
        public string Sku { get; set; }
        public string State { get; set; }
    }
}