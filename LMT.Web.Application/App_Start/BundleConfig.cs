﻿using System.Web.Optimization;

namespace LMT.Web.Application
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            var jsBundle = new Bundle("~/Scripts/js");

            jsBundle
                .Include(
                "~/Scripts/jquery-2.1.0.min.js",
                "~/Scripts/jquery-migrate-1.2.1.min.js",
                "~/Scripts/knockout-3.1.0.js",                
                "~/Scripts/jquery-ui-1.10.4.min.js",                
                "~/Scripts/jquery.validate.min.js", 
                "~/Scripts/jquery.validate.unobtrusive.min.js"
                );

            var jqMobileBundle = new Bundle("~/Scripts/jqMobile");
            jqMobileBundle.Include(
                "~/Scripts/jquery.mobile-1.4.0.min.js",                
                "~/Scripts/jqm-datebox.core.min.js",
                "~/Scripts/jqm-datebox.mode.calbox.min.js",
                "~/Scripts/jquery.mobile.datebox.i18n.it.utf8.js",
                "~/Scripts/jquery.mobile.simpledialog2.min.js"
                );

            var cssBundle = new Bundle("~/Content/css");

            cssBundle.Include(
                "~/Content/jquery.mobile-1.4.0.min.css",
                "~/Content/jquery.mobile.structure-1.4.0.min.css",
                "~/Content/jquery.mobile.theme-1.4.0.min.css",                
                "~/Content/themes/base/jquery.ui.core.css",
                "~/Content/themes/base/jquery.ui.resizable.css",
                "~/Content/themes/base/jquery.ui.selectable.css",
                "~/Content/themes/base/jquery.ui.accordion.css",
                "~/Content/themes/base/jquery.ui.autocomplete.css",
                "~/Content/themes/base/jquery.ui.button.css",
                "~/Content/themes/base/jquery.ui.dialog.css",
                "~/Content/themes/base/jquery.ui.slider.css",
                "~/Content/themes/base/jquery.ui.tabs.css",
                "~/Content/themes/base/jquery.ui.datepicker.css",
                "~/Content/themes/base/jquery.ui.progressbar.css",
                "~/Content/themes/base/jquery.ui.theme.css",
                "~/Content/jqm-datebox.min.css",
                "~/Content/jquery.mobile.simpledialog.min.css",
                "~/Content/site.css");

            bundles.Add(jsBundle);
            bundles.Add(jqMobileBundle);
            bundles.Add(cssBundle);

            BundleTable.EnableOptimizations = true;
        }
    }
}