﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace LMT.Web.Application.Controllers
{
    public class MakitaController : Controller
    {
        public ActionResult Index()
        {
            //HEADERS for all requests
            //Authorization = Basic YWo4MCEzNzk1MTpyMjN0OSE0MmVw
            //Content-Type = application/x-www-form-urlencoded
            //Referer = http://www.makita.it/sol/esplosi.swf
            //Origin = http://www.makita.it
            //Accept = */*
            //Accept-Encoding = gzip,deflate,sdch
            //Accept-Language = it,en-US;q=0.8,en;q=0.6
            //User-Agent = Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36
            //Content-Length = 53
            //Host = www.makita.it


            //PARAMETERS
            //esuser = 108070
            //escdln = 01

            var request = (HttpWebRequest)WebRequest.Create("http://www.makita.it/sol/stampe/stampa.php");

            request.ContentType = "application/x-www-form-urlencoded";

            request.Method = "POST";

            request.Referer = "http://www.makita.it/sol/esplosi.swf";

            Console.Write("Digita il nome dell'esploso da scaricare: ");

            var code = Console.ReadLine();

            var postData = string.Format("escdar={0}&esvers=1", code);

            var data = Encoding.ASCII.GetBytes(postData);

            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
                stream.Write(data, 0, data.Length);

            var response = (HttpWebResponse)request.GetResponse();

            var pdf = new MemoryStream();
            response.GetResponseStream().CopyTo(pdf);

            System.IO.File.WriteAllBytes(string.Format("{0}.pdf", code), pdf.ToArray()); 

            return View();
        }

    }
}
