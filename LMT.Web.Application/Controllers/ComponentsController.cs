﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using ClosedXML.Excel;
using LMT.Data.EF;
using LMT.Web.Application.Models;
using LMT.Web.Application.Properties;
using MvcContrib.Pagination;

namespace LMT.Web.Application.Controllers
{
    [Authorize]
    public class ComponentsController : Controller
    {
        private readonly Entities _entities;

        public ComponentsController(Entities entities)
        {
            _entities = entities;
        }

        public ActionResult Index(int? page, ComponentSearchModel componentSearchModel)
        {
            var components = _entities.Components.Where(c => !c.Deleted);

            if (!string.IsNullOrWhiteSpace(componentSearchModel.Name))
                components = components.Where(c => c.Name.Contains(componentSearchModel.Name));
            
            if (!string.IsNullOrWhiteSpace(componentSearchModel.Sku))
                components = components.Where(c => c.Sku.Contains(componentSearchModel.Sku));

            componentSearchModel.SearchResultModels =
                components.OrderByDescending(c => c.UpdatedOn).ToList().Select(
                    c =>
                        new ComponentReadModel
                        {
                            Id = c.Id,
                            Name = c.Name,
                            Sku = c.Sku,
                            StockQuantity = c.StockQuantity.ToString()
                        });

            componentSearchModel.ComponentGroupReadModels =
                _entities.ComponentGroups.Where(c => !c.Deleted).Include(c => c.ComponentGroupComponents)
                    .OrderByDescending(c => c.UpdatedOn)
                    .Select(
                        c =>
                            new ComponentGroupReadModel
                            {
                                Id = c.Id,
                                Name = c.Name,
                                ComponentGroupComponents = c.ComponentGroupComponents
                            });

            componentSearchModel.SearchResultModels = componentSearchModel.SearchResultModels.AsPagination(page ?? 1, 15);

            ViewBag.AjaxEnabled = true;

            return View(componentSearchModel);
        }

        public FileContentResult Export(ComponentSearchModel componentSearchModel)
        {
            var workbook = new XLWorkbook();
            var workSheet = workbook.AddWorksheet("Componenti");

            workSheet.Column("1").Hide();
            workSheet.Cell("A1").Value = "Id";
            workSheet.Cell("B1").Value = "Codice";
            workSheet.Cell("C1").Value = "Nome";
            workSheet.Cell("D1").Value = "Quantita'";            

            var rngTable = workSheet.Range("A1:D1");

            rngTable.Style
                .Font.SetBold()
                .Fill.SetBackgroundColor(XLColor.CornflowerBlue)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

            rngTable.SetAutoFilter();

            var components =
                _entities.Components.Where(c => !c.Deleted && c.StockQuantity > 0)
                    .OrderBy(c => c.Sku)
                    .Select(c => new {c.Id, c.Sku, c.Name, c.StockQuantity})
                    .ToList();

            workSheet.Cell(2, 1).Value = components.AsEnumerable();

            workSheet.Columns().AdjustToContents();

            var reportStream = new MemoryStream();
            workbook.SaveAs(reportStream);

            return File(reportStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        string.Format("Componenti-{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmm")));
        }

        public ActionResult Create()
        {
            return View(new ComponentCreateModel());
        }

        public PartialViewResult Edit(int componentId)
        {
            var component = _entities.Components.Find(componentId);

            var model = new ComponentEditModel { Id = component.Id, Name = component.Name, Sku = component.Sku };

            return PartialView("_Edit", model);
        }

        public PartialViewResult AddRemoveQuantity(int componentId)
        {
            var component = _entities.Components.Find(componentId);

            var model = new ComponentAddRemoveQuantityModel {Id = component.Id, Name = component.Name, Sku = component.Sku};

            return PartialView("_AddRemoveQuantity", model);
        }

        public PartialViewResult DeleteConfirm(int id)
        {
            var component = _entities.Components.Find(id);

            ViewBag.Id = component.Id;
            ViewBag.ComponentName = component.Name;

            return PartialView("_DeleteConfirm");
        }

        [HttpPost]
        public ActionResult AddRemoveQuantity(ComponentAddRemoveQuantityModel componentAddRemoveQuantityModel)
        {
            var component = _entities.Components.Find(componentAddRemoveQuantityModel.Id);
            component.StockQuantity += decimal.Parse(componentAddRemoveQuantityModel.Quantity);
            component.UpdatedOn = DateTime.Now;

            var componentEntry = new ComponentEntry
            {
                Component = component,
                CreatedOn = DateTime.Now,
                Quantity = decimal.Parse(componentAddRemoveQuantityModel.Quantity),
                Note = componentAddRemoveQuantityModel.Note
            };

            _entities.ComponentEntries.Add(componentEntry);

            _entities.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Create(ComponentCreateModel componentCreateModel)
        {
            if (!ModelState.IsValid)
            {
                return View(componentCreateModel);
            }

            var component = new Component
            {
                CreatedOn = DateTime.Now,
                UpdatedOn = DateTime.Now,
                Name = componentCreateModel.Name,
                Sku = componentCreateModel.Sku
            };

            _entities.Components.Add(component);

            _entities.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(ComponentEditModel componentEditModel)
        {
            var component = _entities.Components.Find(componentEditModel.Id);

            component.Name = componentEditModel.Name;
            component.Sku = componentEditModel.Sku;
            component.Deleted = componentEditModel.Deleted;

            _entities.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult IsValidSku(string sku, int id = 0)
        {
            var componentsCount = id == 0
                ? _entities.Components.Count(c => !c.Deleted && c.Sku == sku)
                : _entities.Components.Count(c => !c.Deleted && c.Sku == sku && c.Id != id);

            return Json(componentsCount == 0);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var component = _entities.Components.Find(id);

            component.Deleted = true;

            _entities.SaveChanges();

            return RedirectToAction("Index");
        }        
    }    
}
