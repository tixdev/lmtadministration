﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using LMT.Data.EF;
using LMT.Web.Application.Models;
using MvcContrib.Pagination;

namespace LMT.Web.Application.Controllers
{
    [Authorize]
    public class ComponentGroupsController : Controller
    {
        private readonly Entities _entities;

        public ComponentGroupsController(Entities entities)
        {
            _entities = entities;
        }

        public ActionResult Index(int? page)
        {
            var models =                            
                _entities.ComponentGroups.Where(c => !c.Deleted).Include(c => c.ComponentGroupComponents)
                    .OrderByDescending(c => c.UpdatedOn)
                    .Select(
                        c =>
                            new ComponentGroupReadModel
                            {
                                Id = c.Id,
                                Name = c.Name,
                                ComponentGroupComponents = c.ComponentGroupComponents
                            });

            ViewBag.AjaxEnabled = true;

            return View(models.AsPagination(page ?? 1, 15));
        }

        public ActionResult Create()
        {
            return View(new ComponentGroupCreateModel());
        }

        public ActionResult Edit(int id)
        {
            var componentGroup = _entities.ComponentGroups.Find(id);

            var components = _entities.Components.Where(c => !c.Deleted).ToList();

            var model = new ComponentGroupEditModel
            {
                Id = componentGroup.Id,
                Name = componentGroup.Name,
                ComponentGroupComponentModels =
                    componentGroup.ComponentGroupComponents.Select(
                        c => new ComponentGroupComponentModel
                        {
                            Quantity = c.Quantity.ToString("0.#"),
                            ComponentId = c.Component.Id,
                            Name = c.Component.Name,
                            Sku = c.Component.Sku
                        }).ToList(),
                Components =
                    components.OrderBy(c => c.Sku).Select(
                        c =>
                            new SelectListItem
                            {
                                Text = string.Format("{0}({1})", c.Sku, c.Name),
                                Value = c.Id.ToString()
                            })
            };

            ViewBag.AjaxEnabled = true;

            return View("Edit", model);
        }

        public PartialViewResult DeleteConfirm(int id)
        {
            var componentGroup = _entities.ComponentGroups.Find(id);
            
            ViewBag.Id = componentGroup.Id;
            ViewBag.ComponentGroupName = componentGroup.Name;

            return PartialView("_DeleteConfirm");
        }

        public ActionResult CheckAvailability()
        {
            ViewBag.AjaxEnabled = true;

            return View();
        }
        
        public JsonResult GetComponentGroups()
        {
            var components =
                _entities.ComponentGroups.Where(c => !c.Deleted)
                    .OrderBy(c => c.Name).ToList()
                    .Select(
                        c =>
                            new
                            {
                                value = c.Id,
                                text = string.Format("{0}({1} componenti)", c.Name, c.ComponentGroupComponents.Count())
                            }).ToList();

            return Json(components, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CalculateAvailability(IEnumerable<ComponentGroupQuantityModel> componentGroupQuantityModels)
        {
            var componentGroupIds = componentGroupQuantityModels.Select(c => c.SelectedComponentGroupId).ToList();

            var componentGroups =
                _entities.ComponentGroups.Where(c => componentGroupIds.Contains(c.Id))
                    .Include(c => c.ComponentGroupComponents)
                    .ToList();

            var components =
                componentGroups.SelectMany(c => c.ComponentGroupComponents).Select(c => c.Component).Distinct();

            var componentsAvailability =
                components.Select(c => new {c.Id, c.Name, c.Sku, c.StockQuantity}).OrderBy(c => c.Sku)
                    .ToList();

            var componentsQuantityNeeded = new Dictionary<Component, decimal>();
            var componentsQuantityRemaining = new Dictionary<Component, decimal>();

            foreach (var componentGroup in componentGroups)
            {
                foreach (var componentGroupComponent in componentGroup.ComponentGroupComponents)
                {
                    var numberOfComponentGroupToBeAssembled =
                        componentGroupQuantityModels.First(c => c.SelectedComponentGroupId == componentGroup.Id)
                            .Quantity;

                    if (!componentsQuantityNeeded.ContainsKey(componentGroupComponent.Component))
                        componentsQuantityNeeded.Add(componentGroupComponent.Component, 0);

                    componentsQuantityNeeded[componentGroupComponent.Component] += componentGroupComponent.Quantity*
                                                                             numberOfComponentGroupToBeAssembled;
                }
            }

            foreach (var keyValue in componentsQuantityNeeded)
            {
                var componentCurrentQuantity = componentsAvailability.First(c => c.Id == keyValue.Key.Id).StockQuantity;

                componentsQuantityRemaining.Add(keyValue.Key, componentCurrentQuantity - keyValue.Value);
            }

            var componentsQuantityNeededModel =
                componentsQuantityNeeded.Select(c => new {c.Key.Id, c.Key.Name, c.Key.Sku, StockQuantity = c.Value})
                    .OrderBy(c => c.Sku);

            var componentsQuantityRemainingModel =
                componentsQuantityRemaining.Select(c => new {c.Key.Id, c.Key.Name, c.Key.Sku, StockQuantity = c.Value})
                    .OrderBy(c => c.Sku);

            return
                Json(
                    new
                    {
                        componentsAvailability,
                        componentsNeedAvailability = componentsQuantityNeededModel,
                        componentsRemainingAvailability = componentsQuantityRemainingModel
                    });
        }

        public PartialViewResult RemoveComponentQuantities()
        {
            return PartialView("_RemoveQuantitiesConfirm");
        }
        
        [HttpPost]
        public ActionResult RemoveComponentQuantities(IEnumerable<ComponentGroupQuantityModel> componentGroupQuantityModels)
        {
            var componentGroupIds = componentGroupQuantityModels.Select(c => c.SelectedComponentGroupId).ToList();

            var componentGroups =
                _entities.ComponentGroups.Where(c => componentGroupIds.Contains(c.Id))
                    .Include(c => c.ComponentGroupComponents)
                    .ToList();

            var componentsQuantityNeeded = new Dictionary<Component, decimal>();            

            foreach (var componentGroup in componentGroups)
            {
                foreach (var componentGroupComponent in componentGroup.ComponentGroupComponents)
                {
                    var numberOfComponentGroupToBeAssembled =
                        componentGroupQuantityModels.First(c => c.SelectedComponentGroupId == componentGroup.Id)
                            .Quantity;

                    if (!componentsQuantityNeeded.ContainsKey(componentGroupComponent.Component))
                        componentsQuantityNeeded.Add(componentGroupComponent.Component, 0);

                    componentsQuantityNeeded[componentGroupComponent.Component] += componentGroupComponent.Quantity *
                                                                             numberOfComponentGroupToBeAssembled;
                }
            }

            foreach (var keyValue in componentsQuantityNeeded)
            {                
                keyValue.Key.StockQuantity -= keyValue.Value;
                keyValue.Key.UpdatedOn = DateTime.Now;

                var componentEntry = new ComponentEntry
                {
                    Component = keyValue.Key,
                    CreatedOn = DateTime.Now,
                    Quantity = keyValue.Value*-1,
                    Note = ""
                };

                _entities.ComponentEntries.Add(componentEntry);
            }

            _entities.SaveChanges();

            return Json(null);
        }        

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var componentGroup = _entities.ComponentGroups.Find(id);

            componentGroup.Deleted = true;

            _entities.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Create(ComponentGroupCreateModel componentGroupCreateModel)
        {
            var componentGroup = new ComponentGroup
            {
                Name = componentGroupCreateModel.Name,
                CreatedOn = DateTime.Now,
                UpdatedOn = DateTime.Now
            };

            _entities.ComponentGroups.Add(componentGroup);

            _entities.SaveChanges();

            return RedirectToAction("Edit", new { id = componentGroup.Id });
        }

        [HttpPost]
        public ActionResult Edit(ComponentGroupEditModel componentGroupEditModel)
        {
            var componentGroup = _entities.ComponentGroups.Find(componentGroupEditModel.Id);

            componentGroup.Name = componentGroupEditModel.Name;

            var components =
                componentGroupEditModel.ComponentGroupComponentModels.Where(
                    c => !string.IsNullOrWhiteSpace(c.Quantity) && decimal.Parse(c.Quantity) > 0).ToList();

            foreach (var componentGroupComponent in componentGroup.ComponentGroupComponents.ToList())
                _entities.ComponentGroupComponents.Remove(componentGroupComponent);

            foreach (var model in components)
                componentGroup.ComponentGroupComponents.Add(new ComponentGroupComponent
                {
                    Component = _entities.Components.Find(model.ComponentId),
                    Quantity = decimal.Parse(model.Quantity)
                });

            componentGroup.Deleted = componentGroupEditModel.Deleted;

            _entities.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}
