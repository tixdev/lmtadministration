﻿using System.Web.Mvc;
using System.Web.Security;
using LMT.Web.Application.Models;

namespace LMT.Web.Application.Controllers
{

    [Authorize]
    public class AccountController : Controller
    {       
        public ActionResult Index()
        {
            return View();
        }        

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }        

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }        

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }        
    }
}
