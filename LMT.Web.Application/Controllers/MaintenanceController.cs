﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using LMT.Data.EF;
using System.Linq;
using LMT.Web.Application.Models;
using LMT.Web.Application.Properties;
using MvcContrib.Pagination;
using ClosedXML.Excel;

namespace LMT.Web.Application.Controllers
{
    [Authorize]
    public class MaintenanceController : Controller
    {
        private readonly Entities _entities;

        public MaintenanceController()
        {
            _entities = new Entities();
        }

        public ActionResult Index(int? page, string searchTerms, bool? searchEnabledStatus, MaintenanceSearchModel maintenanceSearchModel)
        {
            var maintenances = GetMaintenances(maintenanceSearchModel);            

            var maintenanceStates =
                _entities.MaintenanceStates.OrderBy(m => m.DisplayOrder).Select(
                    m => new SelectListItem { Text = m.Name, Value = m.Code }).ToList();

            var maintenanceReadModels =
                maintenances.ToList().OrderByDescending(m => m.UpdateDate).Select(
                    m =>
                    new MaintenanceReadModel
                        {
                            Id = m.Id,
                            UpdatedOn = m.UpdateDate.ToString(CultureInfo.InvariantCulture),
                            CreationDate = m.CreationDate.ToString(CultureInfo.InvariantCulture),
                            CustomerName = m.CustomerName,
                            ModelNumber = m.ModelNumber,
                            BrandName = m.BrandName,
                            Sku = m.ProductSku,
                            State = m.MaintenanceState.Name
                        });

            maintenanceSearchModel.SearchResultModels = maintenanceReadModels.AsPagination(page ?? 1, 15);
            maintenanceSearchModel.MaintenanceStates = maintenanceStates;

            return View(maintenanceSearchModel);
        }

        public FileContentResult SearchExport(MaintenanceSearchModel maintenanceSearchModel)
        {
            var workbook = new XLWorkbook(new MemoryStream(Resources.ReportSearchExportTemplate));
            var workSheet = workbook.Worksheets.First();

            workSheet.Column("1").Hide();
            workSheet.Cell("A1").Value = "Id";
            workSheet.Cell("B1").Value = "Data";
            workSheet.Cell("C1").Value = "Cliente";            
            workSheet.Cell("D1").Value = "Marca";
            workSheet.Cell("E1").Value = "Modello";
            workSheet.Cell("F1").Value = "Matricola";
            workSheet.Cell("G1").Value = "Stato";            

            var rngTable = workSheet.Range("A1:G1");

            rngTable.Style
                .Font.SetBold()
                .Fill.SetBackgroundColor(XLColor.CornflowerBlue)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

            rngTable.SetAutoFilter();

            var maintenances = GetMaintenances(maintenanceSearchModel);

            var maintenanceModels =
                maintenances.ToList().OrderByDescending(m => m.CreationDate).Select(
                    m =>
                    new
                        {
                            m.Id,
                            CreationDate = m.CreationDate.ToString(CultureInfo.InvariantCulture),
                            m.CustomerName,                            
                            m.BrandName,
                            m.ModelNumber,
                            Sku = m.ProductSku,
                            State = m.MaintenanceState.Name
                        });

            workSheet.Cell(2, 1).Value = maintenanceModels.AsEnumerable();

            workSheet.Columns().AdjustToContents();

            var reportStream = new MemoryStream();
            workbook.SaveAs(reportStream);

            return File(reportStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        string.Format("Riparazioni-{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmm")));
        }
        
        public FileContentResult DetailExport(int maintenanceId)
        {
            var maintenance = _entities.Maintenances.Find(maintenanceId);

            var workbook = new XLWorkbook(new MemoryStream(Resources.MaintenanceTemplate));
            var workSheet = workbook.Worksheets.First();
            
            workSheet.Cell("E5").Value = maintenance.CreationDate;
            workSheet.Cell("E6").Value = maintenance.CustomerName;
            workSheet.Cell("E7").Value = !string.IsNullOrWhiteSpace(maintenance.BrandName) ? maintenance.BrandName : "-";
            workSheet.Cell("E8").Value = !string.IsNullOrWhiteSpace(maintenance.ModelNumber) ? maintenance.ModelNumber : "-";
            workSheet.Cell("E9").Value = !string.IsNullOrWhiteSpace(maintenance.ProductSku) ? maintenance.ProductSku : "-"; ;

            for (int i = 0; i < maintenance.MaintenanceItems.Count(); i++)
            {
                var maintenanceItem = maintenance.MaintenanceItems.ElementAt(i);
                workSheet.Cell("A" + (20 + i)).Value = maintenanceItem.Description;
                workSheet.Cell("F" + (20 + i)).Value = maintenanceItem.Price.ToString("#.00");
            }

            workSheet.Cell("A39").Value = string.Format("Manodopera - {0}", maintenance.HandWorkHours);
            workSheet.Cell("F39").Value = maintenance.HandWorkPrice;

            var totalPrice = maintenance.MaintenanceItems.Select(m => m.Price).Sum() + maintenance.HandWorkPrice;

            workSheet.Cell("F40").Value = totalPrice;

            var reportStream = new MemoryStream();
            workbook.SaveAs(reportStream);

            return File(reportStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        string.Format("Riparazione-{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmm")));
        }

        public ActionResult Create()
        {
            var maintenanceStates =
                _entities.MaintenanceStates.OrderBy(m => m.DisplayOrder).Select(
                    m => new SelectListItem {Text = m.Name, Value = m.Code}).ToList();

            var model = new MaintenanceCreateModel { CreationDate = DateTime.Now.ToString("dd-MM-yyyy"), MaintenanceStates = maintenanceStates };

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var maintenance = _entities.Maintenances.Find(id);

            var model = GetMaintenanceEditModel(maintenance);
            
            if (Request.UrlReferrer != null) 
                model.ReturningUrl = Request.UrlReferrer.OriginalString;

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(MaintenanceCreateModel maintenanceCreateModel)
        {
            if(!ModelState.IsValid)
            {
                var maintenanceStates =
                   _entities.MaintenanceStates.Select(m => new SelectListItem { Text = m.Name, Value = m.Code }).ToList();

                maintenanceCreateModel.MaintenanceStates = maintenanceStates;

                return View(maintenanceCreateModel);
            }

            var selectedMaintenanceStateCode = maintenanceCreateModel.SelectedMaintenanceState.Value.ToString(CultureInfo.InvariantCulture);
            var maintenanceState =
                _entities.MaintenanceStates.First(
                    m => m.Code == selectedMaintenanceStateCode);

            var maintenance = new Maintenance
                                  {
                                      BrandName = maintenanceCreateModel.BrandName,
                                      CreationDate = DateTime.Now,
                                      UpdateDate = DateTime.Now,
                                      CustomerName = maintenanceCreateModel.Customer,
                                      MaintenanceState =
                                          maintenanceState,
                                      ModelNumber = maintenanceCreateModel.Model,
                                      ProductSku = maintenanceCreateModel.Sku,
                                      HandWorkHours = maintenanceCreateModel.HandWorkHours,
                                      HandWorkPrice =
                                          decimal.Parse(string.IsNullOrWhiteSpace(maintenanceCreateModel.HandWorkPrice)
                                                            ? "0"
                                                            : maintenanceCreateModel.HandWorkPrice)
                                  };

            foreach (var maintenanceItemModel in maintenanceCreateModel.MaintenanceItemModels)
            {
                var price =
                    decimal.Parse(string.IsNullOrWhiteSpace(maintenanceItemModel.MaintenancePrice)
                                      ? "0"
                                      : maintenanceItemModel.MaintenancePrice);

                if(string.IsNullOrWhiteSpace(maintenanceItemModel.MaintenanceDescription) && price == 0)
                    continue;

                var maintenanceItem = new MaintenanceItem
                                          {
                                              Description = maintenanceItemModel.MaintenanceDescription ?? "-",
                                              Price = price,
                                              Maintenance = maintenance
                                          };

                maintenance.MaintenanceItems.Add(maintenanceItem);
            }

            _entities.Maintenances.Add(maintenance);
            _entities.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(MaintenanceEditModel maintenanceEditModel)
        {
            if(!ModelState.IsValid)
            {
                var maintenanceStates =
                   _entities.MaintenanceStates.Select(m => new SelectListItem { Text = m.Name, Value = m.Code }).ToList();

                maintenanceEditModel.MaintenanceStates = maintenanceStates;

                return View(maintenanceEditModel);
            }

            var selectedMaintenanceStateCode = maintenanceEditModel.SelectedMaintenanceState.Value.ToString(CultureInfo.InvariantCulture);
            var maintenanceState =
                _entities.MaintenanceStates.First(
                    m => m.Code == selectedMaintenanceStateCode);

            var maintenance = _entities.Maintenances.Find(maintenanceEditModel.Id);

            maintenance.BrandName = maintenanceEditModel.BrandName;            
            maintenance.UpdateDate = DateTime.Now;           
            maintenance.CustomerName = maintenanceEditModel.Customer;
            maintenance.MaintenanceState = maintenanceState;
            maintenance.ModelNumber = maintenanceEditModel.Model;
            maintenance.ProductSku = maintenanceEditModel.Sku;
            maintenance.HandWorkHours = maintenanceEditModel.HandWorkHours;
            maintenance.HandWorkPrice =
                decimal.Parse(string.IsNullOrWhiteSpace(maintenanceEditModel.HandWorkPrice)
                                  ? "0"
                                  : maintenanceEditModel.HandWorkPrice);

            if (!maintenance.WarrantyStartDate.HasValue && (maintenanceState.Code == MaintenanceState.Delivered || maintenanceState.Code == MaintenanceState.Invoice))
                maintenance.WarrantyStartDate = DateTime.Now;

            foreach (var maintenanceItem in maintenance.MaintenanceItems.ToList())
                _entities.MaintenanceItems.Remove(maintenanceItem);

            foreach (var maintenanceItemModel in maintenanceEditModel.MaintenanceItemModels)
            {
                var price =
                    decimal.Parse(string.IsNullOrWhiteSpace(maintenanceItemModel.MaintenancePrice)
                                      ? "0"
                                      : maintenanceItemModel.MaintenancePrice);

                if (string.IsNullOrWhiteSpace(maintenanceItemModel.MaintenanceDescription) && price == 0)
                    continue;

                var maintenanceItem = new MaintenanceItem
                                          {
                                              Description = maintenanceItemModel.MaintenanceDescription ?? "-",
                                              Price = price,
                                              Maintenance = maintenance
                                          };

                maintenance.MaintenanceItems.Add(maintenanceItem);
            }
            
            _entities.SaveChanges();

            if (!string.IsNullOrWhiteSpace(maintenanceEditModel.ReturningUrl))
                return Redirect(maintenanceEditModel.ReturningUrl);

            return RedirectToAction("Index");
        }

        private IEnumerable<Maintenance> GetMaintenances(MaintenanceSearchModel maintenanceSearchModel)
        {
            var maintenances = _entities.Maintenances.Where(m => !m.Deleted);

            if (!string.IsNullOrWhiteSpace(maintenanceSearchModel.SelectedMaintenanceState))
                maintenances =
                    maintenances.Where(m => m.MaintenanceState.Code == maintenanceSearchModel.SelectedMaintenanceState);

            if (!string.IsNullOrWhiteSpace(maintenanceSearchModel.CustomerName))
                maintenances =
                    maintenances.Where(
                        m => m.CustomerName != null && m.CustomerName.Contains(maintenanceSearchModel.CustomerName));

            if (!string.IsNullOrWhiteSpace(maintenanceSearchModel.Brand))
                maintenances =
                    maintenances.Where(m => m.BrandName != null && m.BrandName.Contains(maintenanceSearchModel.Brand));

            if (!string.IsNullOrWhiteSpace(maintenanceSearchModel.Model))
                maintenances =
                    maintenances.Where(
                        m => m.ModelNumber != null && m.ModelNumber.Contains(maintenanceSearchModel.Model));

            if (!string.IsNullOrWhiteSpace(maintenanceSearchModel.StartDate) &&
                !string.IsNullOrWhiteSpace(maintenanceSearchModel.EndDate))
            {
                var startDate = DateTime.Parse(maintenanceSearchModel.StartDate).Date;
                var endDate = DateTime.Parse(maintenanceSearchModel.EndDate).Date.AddHours(23).AddMinutes(59);

                maintenances =
                    maintenances.ToList().Where(m => m.CreationDate > startDate && m.CreationDate < endDate).AsQueryable
                        ();
            }

            if (!string.IsNullOrWhiteSpace(maintenanceSearchModel.Sku))
                maintenances =
                    maintenances.Where(m => m.ProductSku != null && m.ProductSku.Contains(maintenanceSearchModel.Sku));

            if (maintenanceSearchModel.ExcludeClosedStates == "on")
                maintenances = maintenances.Where(m => m.MaintenanceState.Code != MaintenanceState.Invoice);

            return maintenances;
        }

        private MaintenanceEditModel GetMaintenanceEditModel(Maintenance maintenance)
        {
            var maintenanceStates =
                _entities.MaintenanceStates.OrderBy(m => m.DisplayOrder).Select(
                    m => new SelectListItem {Text = m.Name, Value = m.Code}).ToList();

            var model = new MaintenanceEditModel
                            {
                                Id = maintenance.Id,
                                CreationDate = maintenance.CreationDate.ToString("dd-MM-yyyy"),
                                WarrantyStartDate = maintenance.WarrantyStartDate,
                                MaintenanceStates = maintenanceStates,
                                BrandName = maintenance.BrandName,
                                Customer = maintenance.CustomerName,
                                HandWorkHours = maintenance.HandWorkHours,
                                HandWorkPrice = maintenance.HandWorkPrice.ToString(CultureInfo.InvariantCulture),
                                Sku = maintenance.ProductSku,
                                Model = maintenance.ModelNumber,
                                SelectedMaintenanceState = int.Parse(maintenance.MaintenanceState.Code),
                                MaintenanceItemModels =
                                    maintenance.MaintenanceItems.ToList().Select(
                                        m =>
                                        new MaintenanceItemModel
                                            {
                                                MaintenanceDescription = m.Description,
                                                MaintenancePrice = m.Price.ToString("0.00")
                                            }).ToList()
                            };
            return model;
        }
    }
}