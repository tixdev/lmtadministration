﻿using System.Web.Mvc;
using System.Web.Security;
using LMT.Data.EF;
using System.Linq;
using MvcContrib.Pagination;

namespace LMT.Web.Application.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.IsAdmin = Roles.IsUserInRole(Role.Administrator);
            return View();
        }

        public ActionResult Products(int page = 1, string code = "", string name = "")
        {
            var entities = new Entities();

            var products =
                entities.Products.Where(p => p.Code.Contains(code) && p.Name.Contains(name)).OrderBy(
                    p => p.Code);

            var model =
                products.AsPagination(page, 30);

            ViewBag.Page = page;
            ViewBag.Name = name;
            ViewBag.Code = code;
            ViewBag.VersionDate = entities.Versions.OrderByDescending(v => v.DateTime).First().DateTime;
            ViewBag.IsAdmin = Roles.IsUserInRole(Role.Administrator);

            return View(model);
        }

        [HttpPost]
        public ActionResult Products(string code = "", string name = "")
        {
            return RedirectToAction("Products", new {page = 1, code = code, name = name});
        }       
    }
}
